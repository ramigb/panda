# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
worlds = World.create([
  {title: 'التقنية', icon: 'fa fa-signal'},
  {title: 'الموسيقى', icon: 'fa fa-music'},
  {title: 'الأخبار', icon: 'fa fa-globe'},
  {title: 'الفيديو', icon: 'fa fa-video-camera'},
  {title: 'الصور', icon: 'fa fa-camera'},
])

user = User.create({
  name: 'ramigb',
  email: 'r@seenlab.com',
  password: 'danger123',
  password_confirmation: 'danger123',
  admin: 1
})