class CreateWorlds < ActiveRecord::Migration
  def change
    create_table :worlds do |t|
      t.string :title
      t.text :about
      t.text :rules
      t.text :css
      t.attachment :image
      t.string :icon
      t.integer :posts_count
      t.string :slug

      t.timestamps
    end

    add_index :worlds, :slug, unique: true
  end
end
