class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string   :title, limit: 500
      t.string   :link
      t.string   :slug, null: true
      t.boolean  :is_local, default: 0
      t.integer  :user_id, default: 1
      t.integer  :world_id
      t.attachment :image
      t.boolean   :deleted, default: 0
      t.integer  :comments_count, default: 0
      t.datetime :created_at
      t.datetime :updated_at
    end

    add_index :posts, :slug, unique: true
  end
end
