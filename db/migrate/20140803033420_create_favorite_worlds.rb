class CreateFavoriteWorlds < ActiveRecord::Migration
  def change
    create_table :favorite_worlds, id: false do |t|
      t.integer :world_id
      t.integer :user_id, default: 0
    end
    add_index :favorite_worlds, [:user_id,:world_id]
  end
end
