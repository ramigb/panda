json.extract! @user, :id, :name

json.posts do
  json.array! @user.posts do |post|
    json.partial! post
  end
end
