json.extract! @comment, :id, :body, :flagged, :created_at, :updated_at
json.user do
  json.name @comment.user.name
end
