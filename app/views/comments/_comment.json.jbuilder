json.id comment.id
json.body comment.body
json.flagged comment.flagged
json.parent_id comment.parent_id
json.created_at comment.created_at
json.url comment_url(comment, format: :json)
json.user do
  json.id comment.user.id unless comment.user.nil?
  json.name comment.user.name unless comment.user.nil?
end