def render_comments json, comments
  json.array!(comments) do |comment,nested_comments|
    json.partial! comment
    if nested_comments.size > 0
      json.comments do
        render_comments json, nested_comments
      end
    else
      json.comments nil
    end
  end
end

render_comments json, @comments


