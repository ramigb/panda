json.(post,:id,:title,:created_at,:link,:slug,:is_local,:comments_count)

json.image post.image.url(:thumb)

json.user do
  json.id post.user.id
  json.name post.user.name
end

unless post.world.nil?
  json.world do
    json.title post.world.title
    json.slug post.world.slug
    json.icon post.world.icon
  end
end

json.content do
  json.body post.content.body unless post.content.nil?
end

json.votes post.cached_votes_score