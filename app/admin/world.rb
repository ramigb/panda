ActiveAdmin.register World do


  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #

  permit_params do
   permitted = ["title","about","rules","css","image","icon","slug"]
   permitted
  end
  before_filter :only => [:show, :edit, :update, :destroy] do
    @world = World.find_by_slug(params[:id])
  end


end
