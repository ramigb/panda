// Resources

PandaApp.factory("Post", function($resource) {
  var resourceUrl = ROOT_URL + "posts/:slug";
  return $resource(resourceUrl, { id: "@slug" },
  {
    'create':  { method: 'POST' },
    'index':   { method: 'GET', isArray: true },
    'show':    { method: 'GET', isArray: false },
    'destroy': { method: 'DELETE' }
  }
  );
});

PandaApp.factory("Comment", function($resource) {
  var resourceUrl = ROOT_URL + "comments/:id";
  return $resource(resourceUrl, { id: "@id" },
  {
    'create':  { method: 'POST' },
    'index':   { method: 'GET', isArray: true },
    'show':    { method: 'GET', isArray: false },
    'update':  { method: 'PUT' },
    'destroy': { method: 'DELETE' }
  }
  );
});

PandaApp.factory("World", function($resource) {
  return $resource(ROOT_URL + "worlds/:slug", { id: "@slug" },
  {
    'index':   { method: 'GET', isArray: true },
    'show':    { method: 'GET', isArray: false },
  }
  );
});

PandaApp.factory("User", function($resource) {
  return $resource(ROOT_URL + "users/:id", { id: "@id" },
  {
    'index':   { method: 'GET', isArray: true },
    'show':    { method: 'GET', isArray: false },
  }
  );
});