var PandaApp = angular.module('PandaApp', ['ngRoute', 'ngResource', 'angularMoment',
  'Devise', 'cgNotify', 'ui.select2', 'ngSanitize', 'ngCookies'
]);

PandaApp.config(['$httpProvider', '$locationProvider',
  function($httpProvider, $locationProvider) {
    //So i don't have to add .json file
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

    $httpProvider.interceptors.push(function($q) {
      return {
        'response': function(response) {
          //Will only be called for HTTP up to 300
          return response;
        },
        'responseError': function(rejection) {
          if (rejection.status === 401) {
            return rejection.data;
          }
          return $q.reject(rejection);
        }
      };
    });

  }
]);

PandaApp.config(['$locationProvider',
  function($locationProvider) {
    $locationProvider
      .html5Mode(true);
  }
]);


PandaApp.run(function(amMoment) {
  amMoment.changeLanguage('ar');
});


// Directives

PandaApp.directive('postLink', function() {
  return {
    link: function(scope, postElement, attrs) {
      if (scope.post.is_local) {
        $(postElement).attr('href', 'p/' + scope.post.slug)
      } else {
        $(postElement).attr('href', scope.post.link).attr('target', '_blank');
      }
    }
  };
});

PandaApp.directive('postImage', function() {
  return {
    link: function(scope, elem, attrs) {
      src = attrs.postImage;

      elem.bind('error', function(e) {
        elem.attr("src", 'logo.png');
      });

      elem.attr('src', src);
    }
  };
});

PandaApp.directive('closeSidebar', function() {
  return {
    link: function(scope, elem, attrs) {
      elem.on('click', function() {
        hideSideBar();
      })
    }
  }
})

PandaApp.directive('toggleSidebar', function() {
  return {
    link: function(scope, elem, attrs) {
      elem.on('click', function() {
        toggleSideBar();
      })
    }
  }
})

PandaApp.filter('domainName', function() {
  return function(input) {
    if (!input) return 'panda';
    hostname = new URL(input).hostname.replace('www.', '');
    return hostname;
  }
})

PandaApp.directive('checkImgWidth', function() {
  return {
    restrict: 'A',
    link: function(scope, elem, attr) {
      elem.on('load', function() {
        if ($(this).width() < 100 || $(this).height() < 100) {
          $(this).hide();
        }

        //check width and height and apply styling to parent here.
      });
    }
  };
});


// Angular Specific function

function isAction(actionName, locationObject) {
  var pattern = new RegExp(actionName);
  return pattern.test(locationObject.path());
}