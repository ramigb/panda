PandaApp.controller('UsersCtrl', ['$scope','$location','$http','Auth', 'UserService','action','notify', '$routeParams','User'
                              ,function($scope,$location,$http,Auth, UserService,action,notify,$routeParams,User){

  pageTitle('مرحباً بك')

  $scope.user = UserService.user();

  switch(action) {
      case 'logout':
          pageTitle('تسجيل خروج')
          Auth.logout();
          break;
      case 'profile':
          User.get({id: $routeParams.id}).$promise.then(function(user){
            $scope.user = user;
            $scope.posts = user.posts;
          })
      case 'reset_password':
        if($location.search() && $location.search().reset_password_token){
          $scope.User = {};
          $scope.User.reset_password_token = $location.search().reset_password_token
        }
      break;
      default:

  }



  $scope.login = function(user){
      if(user == undefined){
        notify('Must enter name and password');
        return false;
      };

      var credentials = {
        name: user.name,
        password: user.password
      };

      Auth.login(credentials).then(function(user) {
          if(user == undefined){
            notify('Wrong user and/or password!')
            return false;
          }
          $location.path('/');
          UserService.user(user);
        }, function(error) {
          notify('Wrong user and/or password!')
        });
  }

  $scope.register = function(user){
      if(user == undefined){
        notify('Must enter email and password');
        return false;
      };

      var credentials = {
        name: user.name,
        email: user.email,
        password: user.password,
        password_confirmation: user.password_confirmation,
      };

      Auth.register(credentials).then(function(user) {
          $location.path('/');
          UserService.user(user);
        }, function(error) {
          var errorMessage = ""
          $.each(error.data.errors, function(index,value){
            errorMessage += index + " : " + value + "\n\r";
          })
          notify(errorMessage)
        });
  }

  $scope.forgotPassword = function(user){
    $http.post(ROOT_URL + 'users/password', {user: user}).success(function(data){
      console.log(data);
    })
  }

  $scope.resetPassword = function(user){
    $http.put(ROOT_URL + 'users/password', {user: user}).success(function(data){
      console.log(data);
    })
  }

  // Events
  $scope.$on('devise:logout', function(event, currentUser) {
          // after a login, a hard refresh, a new tab
          notify('Logged out');
          UserService.user(null);
  });


}]) //End of Controller