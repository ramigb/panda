PandaApp.controller('CommentsCtrl', ['$scope', '$http', '$location',
  'Comment', '$routeParams',
  function($scope, $http, $location, Comment, $routeParams) {

    $scope.comments = [];
    $scope.Comment = {};
    $scope.CommentReply = {};
    $scope._replyTo = {};

    Comment.index({
      commentable_type: $scope.commentable.type,
      commentable_id: $scope.commentable.id
    }).$promise.then(function(comments) {
      $scope.comments = comments;
    })


    $scope.createComment = function(comment) {
      comment.commentable_type = $scope.commentable.type;
      comment.commentable_id = $scope.commentable.id;
      Comment.create({
        comment: comment
      }, function(c) {
        c._class = 'new_comment';
        $scope.comments.unshift(c);
        $scope.Comment = {};
      })
    }

    $scope.createReplyComment = function(comment) {
      comment.commentable_type = $scope.commentable.type;
      comment.commentable_id = $scope.commentable.id;
      Comment.create({
        comment: comment
      }, function(c) {
        if ($scope._replyTo.comments == null) $scope._replyTo.comments = [];
        c._class = 'new_comment';
        $scope._replyTo.comments.unshift(c);
        $scope.CommentReply = {};
        $('#reply_form').hide();
      })
    }

    $scope.replyToComment = function(comment) {
      $('#reply_form').show();
      $scope.CommentReply.parent_id = comment.id;
      $scope._replyTo = comment;
      $('#reply_form').appendTo($('#comment_' + comment.id));
    }

    $scope.cancelReply = function() {
      $scope.CommentReply = {}
      $('#reply_form').hide();
    }

    $scope.moreComments = function() {}


    $scope.vote = function(comment) {
      $http.get(ROOT_URL + 'comments/' + comment.slug + '/vote').success(function(data) {
        if (data == 'true') comment.votes += 1;
      })
    }
  }
]);