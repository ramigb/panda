PandaApp.controller('PostsCtrl', ['$scope', '$http', '$location',
  'Post', '$routeParams', 'WorldsService', 'PostsService', 'User',
  function($scope, $http, $location, Post, $routeParams, WorldsService, PostsService, User) {

    PostsService.world(false);
    PostsService.posts().$promise.then(function(posts) {
      $scope.posts = posts;
      if (posts.length == 0) $scope.no_posts = true;
    });


    $scope.commentable = {};

    $scope.morePosts = function() {
      return PostsService.morePosts();
    }


    $scope.vote = function(post) {
      $http.get(ROOT_URL + 'posts/' + post.slug + '/vote').success(function(data) {
        if (data == 'true') post.votes += 1;
      })
    }



    if ($routeParams.slug) {
      Post.get({
        slug: $routeParams.slug
      }).$promise.then(function(p) {
        // For comments
        $scope.post = p;
        $scope.commentable.type = 'Post';
        $scope.commentable.id = p.id;
        pageTitle(p.title)
      })
    } else {
      pageTitle();
    }

    // Events

    $scope.$on('newPosts', function(e) {
      $scope.posts = PostsService.cachedPosts();
    })

    $scope.set_add_new_text("");


  }
]);