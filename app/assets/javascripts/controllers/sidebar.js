PandaApp.controller('SideBarCtrl',['$scope','$cookies','WorldsService','UserService','PostsService',function($scope,$cookies,WorldsService,UserService,PostsService){
  // Keep Slide Bar Open?
  $scope.barOpen = $cookies.keepBarOpen == 'true' ? true : false;
  keepBarOpen = $scope.barOpen;


  //Get worlds
  WorldsService.worlds().$promise.then(function(worlds){
    $scope.worlds = worlds;
  });

  $scope.postsOrder = function(){
    return PostsService.postsOrder();
  }

  $scope.setPostsOrder = function(order){
    PostsService.postsOrder(order);
    PostsService.reset();
  }

  $scope.ifOrder = function(order){
    if(order == $scope.postsOrder()){
      return 'active';
    }
  }

  //Watch for user
  $scope.$watch(
    function(){
      return UserService.user()
    }
    ,
    function(newValue,OldValue){
      if ( newValue !== OldValue ) {
        $scope.user   = newValue;
      }
    }
  );

  //Keep bar open
  $scope.keepBarOpen = function(){
    if(keepBarOpen){
      keepBarOpen = false
    }else{
      keepBarOpen = true;
    }
    //Update cookies.
    $cookies.keepBarOpen = keepBarOpen;
  }

  $scope.ifKeepBarOpen = function(){
    return $cookies.keepBarOpen;
  }

  $scope.initUserPrefs = function(){
    // SlideBar
    if($scope.barOpen){
      showSideBar();
    }
  }

  $scope.initUserPrefs();
}]);