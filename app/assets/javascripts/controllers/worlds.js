var _location = false;

PandaApp.controller('WorldsCtrl', ['$scope', '$http', '$location',
  'Post', '$routeParams', 'World', 'PostsService',
  function($scope, $http, $location, Post, $routeParams, World, PostsService) {

    $scope.morePosts = function() {
      PostsService.morePosts({
        world: $scope.world.id
      });
    }

    if ($routeParams.slug) {
      $scope.world = World.get({
        slug: $routeParams.slug
      }, function(world) {
        PostsService.world(world.id);
        PostsService.posts({
          world: world.id
        }).$promise.then(function(posts) {
          $scope.posts = posts;
          if (posts.length == 0) $scope.no_posts = true;
        });
        pageTitle(world.title)
        $scope.set_add_new_text(world.title);
      })
    }

    if ($scope.isAction('ws')) {
      $scope.worlds = World.index();
    }
    // Events

    $scope.$on('newPosts', function(e) {
      $scope.posts = PostsService.cachedPosts();
    })

  }
]);