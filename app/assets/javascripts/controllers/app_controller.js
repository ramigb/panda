PandaApp.controller('AppCtrl', ['$location', '$scope', 'StringsService',
  function($location, $scope, StringsService) {
    $scope.main_page = 'Hello Rami';

    $scope.add_new_text = function() {
      return StringsService.add_new_text();
    }

    $scope.set_add_new_text = function(toWhat) {
      StringsService.set_add_new_text(toWhat);
    }

    // This function checks if the action called matches the URL, could be improved of course.
    $scope.isAction = function(actionName) {
      var pattern = new RegExp(actionName);
      return pattern.test($location.path());
    }
  }
])