PandaApp.controller('NewWorldCtrl',['$scope' , '$http' , '$location',
 'Post', '$routeParams' , 'WorldsService', 'PostsService', 'User'
 ,function($scope,$http,$location,Post,$routeParams,WorldsService,PostsService,User){

  pageTitle('أضف عالم جديد');


  $scope.worlds = WorldsService.worlds();

  $scope.select2Options = {};

  $scope.Post = {world_id: PostsService.world()}

  $scope.createPost = function(post){
    // Form validation
    console.log(post);

    if(!$scope.postForm.$valid)
      return false;

    // No link or body?
    if(!post.content_attributes && !post.link)
      return false;

    // Finally maybe the idiot is still running ...
    if(!post.world_id)
      return false;

    Post.create({post: post},function(p){
      $location.path('p/' + p.slug);
    })

  }


  $scope.linkIsThere = function(){
    if(!$scope.Post || $scope.Post.link == undefined) return false;
    return $scope.Post.link.length > 5;
  }

  $scope.fetchTitle = function(post){
    $http.get(ROOT_URL + 'util/get_title',{params: {link: post.link}}).success(function(data){
      if(data.length > 0){
        post.title = data;
      }
    })
  }

  $scope.fetchThumb = function(post){
    $scope.suggestedImages = [];
    $http.get(ROOT_URL + 'util/get_thumb',{params: {link: post.link}}).success(function(data){
      if(data.length > 0){
        $scope.suggestedImages = data;
      }
    })
  }

  $scope.setImage = function(post,img,e){
    if($('.selected_thumb').length > 0)
      $('.selected_thumb').removeClass('selected_thumb');

    $(e.currentTarget).addClass('selected_thumb');

    post.image = img;
  }

  if($routeParams.slug){
    Post.get({slug: $routeParams.slug}).$promise.then(function(p){
    // For comments
      $scope.post = p;
      $scope.commentable.type  = 'Post';
      $scope.commentable.id    =  p.id;
    })

  }


}]);