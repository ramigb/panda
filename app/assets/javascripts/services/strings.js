PandaApp.service('StringsService', ['$http',
  function($http) {

    var _add_new_string = "أضف رابط جديد";
    var _to_what_string = "";
    var _to_what_prefix_string = " إلى ";


    this.set_add_new_text = function(toWhat) {
      _to_what_string = toWhat;
    }

    this.add_new_text = function() {
      if (_to_what_string.length) {
        return _add_new_string + _to_what_prefix_string + _to_what_string;
      } else {
        return _add_new_string;
      }
    }
  }
]);