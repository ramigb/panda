PandaApp.service('WorldsService',['$http','World',function($http,World){
  var _worlds = [];

  function _get_worlds(){
    if(_worlds.length < 1){
      _worlds = World.index();
    }
  }

  this.worlds = function(){
    _get_worlds();
    return _worlds;
  }
}])