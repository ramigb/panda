PandaApp.service('UserService', ['$http', function($http){
  var _user = false;

  this.user = function(user){
    if(user !== false && user !== undefined) _user = user;
    return _user;
  }

  var _init = function(){
    if(!_user){
      $http.get(ROOT_URL + 'get_user').success(function(user){
        user = user === 'null' ? false : user;
        _user = user;
      })
    }
  }

  _init();
}])