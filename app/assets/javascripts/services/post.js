PandaApp.service('PostsService',['$http','$cookies','Post','$rootScope',function($http,$cookies,Post,$rootScope){
  var _posts = [];
  var _world = false;
  var _page = 1;
  var _posts_order = $cookies.postsOrder ? $cookies.postsOrder : 'hot';
  var that = this;

  //This is the class initilizer, somehow! so it resets stuff each time it's called :)
  this.posts = function(){
    _page = 1;
    hashOptions = prepHash();
    _posts = Post.index(hashOptions);
    $rootScope.$broadcast('newPosts');
    return _posts;
  }

  //Gets and appends more posts to the _posts array
  this.morePosts = function(){
    _page += 1;

    hashOptions = prepHash();

    Post.index(hashOptions).$promise.then(function(data){
      if(data.length > 0){
        data.map(function(x){ _posts.push(x) })
      }
    })
  }

  // Setters and Getters

  //Getter
  this.cachedPosts = function(){
    return _posts;
  }

  //Setter & Getter
  this.postsOrder = function(order){
    if(order == undefined) return _posts_order;
    _posts_order = order;
    $cookies.postsOrder = order;
  }

  //Setter & Getter
  this.page = function(page){
    if(page == undefined) return _page;
    _page = page;
  }

  //Setter & Getter
  this.world = function(world){
    if(world == undefined) return _world;
    _world = world;
  }

  //reSetter
  this.reset = function(){
    _posts = [];
    that.posts();
  }

  //Inject hash
  var prepHash = function(){
    hashOptions = {};
    if(_page) hashOptions.page = _page;
    if(_posts_order) hashOptions.order = _posts_order;
    if(_world) hashOptions.world = _world
    return hashOptions;
  }
}])