// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require moment-with-langs
//= require bootstrap
//= require angular
//= require angular-route
//= require angular-cookies
//= require angular-resource
//= require angular-moment
//= require angular.ui.select2
//= require select2
//= require select2_locale_ar
//= require angular-sanitize
//= require devise
//= require angular-notify



//= require ang-app
//= require resources/index
//= require services/index
//= require controllers/index
//= require routes

var sideBar = false;
var keepBarOpen = false;

var showSideBar = function() {
  if (sideBar) return false;

  $('.mainbar').css('right', -300);
  $('.mainbar').css('display', 'block');
  // $('.view_area').animate({right: 300});
  $('.mainbar').animate({
    right: 0
  })
  sideBar = true;

}

var hideSideBar = function() {
  if (!sideBar) return false;
  if (keepBarOpen) return false;

  $('.mainbar').animate({
    right: -300
  })
  // $('.view_area').animate({right: 0});
  sideBar = false;
}

var toggleSideBar = function(e) {
  if (keepBarOpen) return false;

  if (!sideBar) {
    showSideBar();
  } else {
    hideSideBar();
  }
};

var ready = function() {} //End of ready
$(document).ready(ready);

var pageTitle = function(title) {
  originalTitle = 'صفر';
  if (title == undefined) {
    $(document).attr('title', originalTitle);
  } else {
    $(document).attr('title', originalTitle + ' - ' + title);
  }
}

// Later this should go to utility.js

function youtube_link(url) {
  var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
  var match = url.match(regExp);
  if (match && match[2].length == 11) {
    return match[2];
  } else {
    return '';
  }
}