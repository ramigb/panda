PandaApp.config(['$routeProvider', function($routeProvider){
	$routeProvider

		.when('/', {
			templateUrl: ROOT_URL + 'partials/start.html',
			controller: 'PostsCtrl'
		})

    .when('/new',{
      templateUrl: ROOT_URL + 'partials/posts/new.html',
      controller: 'NewPostCtrl',
      resolve: {
        UsersService: function(){
          console.log();
        }
      }
    })

    .when('/login',{
      templateUrl: ROOT_URL + 'partials/users/login.html',
      controller: 'UsersCtrl',
      resolve: {
        action: function(){
          return 'login';
        }
      }
    })

    .when('/register',{
      templateUrl: ROOT_URL + 'partials/users/register.html',
      controller: 'UsersCtrl',
      resolve: {
        action: function(){
          return 'register';
        }
      }
    })


    .when('/logout',{
      templateUrl: ROOT_URL + 'partials/users/login.html',
      controller: 'UsersCtrl',
      resolve: {
        action: function(){
          return 'logout';
        }
      }
    })

    .when('/forgot_password',{
      templateUrl: ROOT_URL + 'partials/users/forgot_password.html',
      controller: 'UsersCtrl',
      resolve: {
        action: function(){
          return 'forgot_password';
        }
      }
    })

    .when('/users/password/edit',{
      templateUrl: ROOT_URL + 'partials/users/reset_password.html',
      controller: 'UsersCtrl',
      resolve: {
        action: function(){
          return 'reset_password';
        }
      }
    })

    .when('/me',{
      templateUrl: ROOT_URL + 'partials/users/show.html',
      controller: 'UsersCtrl',
      resolve: {
        action: function(){
          return 'profile';
        }
      }
    })

    .when('/u/:id',{
      templateUrl: ROOT_URL + 'partials/users/show.html',
      controller: 'UsersCtrl',
      resolve: {
        action: function(){
          return 'profile';
        }
      }
    })

		.when('/p/:slug', {
			templateUrl: ROOT_URL + 'partials/posts/show.html',
			controller: 'PostsCtrl'
		})

    .when('/w/:slug',{
      templateUrl: ROOT_URL + 'partials/worlds/show.html',
      controller: 'WorldsCtrl'
    })

    .when('/ws',{
      templateUrl: ROOT_URL + 'partials/worlds/index.html',
      controller: 'WorldsCtrl'
    })

    .when('/ws/new',{
      templateUrl: ROOT_URL + 'partials/worlds/new.html',
      controller: 'NewWorldCtrl'
    })

    .when('/page/:id',{
      templateUrl: function(routeParams){
        return ROOT_URL + 'partials/pages/' + routeParams.id + '.html'
      }
    })

    //Hushhh and let Rails handle it :)
		// .otherwise({
		// 	redirectTo: '/'
		// });
}]);
