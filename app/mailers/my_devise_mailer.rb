class MyDeviseMailer < Devise::Mailer
  def reset_password_instructions(record)
    mandrill = Mandrill::API.new("#{MandrillConfig.api_key}")
    mandrill.messages 'send-template',
            {
              :template_name => 'Forgot Password',
              :template_content => "",
              :message => {
                :subject => "Forgot Password",
                :from_email => "info@7bab.com",
                :from_name => "Company Support",
                :to => [
                  {
                    :email => record.email
                  }
                ],
                :global_merge_vars => [
                  {
                    :name => "FIRST_NAME",
                    :content => record.name
                  },
                  {
                    :name => "FORGOT_PASSWORD_URL",
                    :content => "<a href='#{edit_user_password_url(:reset_password_token => record.reset_password_token)}'>Change My Password</a>"
                  }
                ]
              }
            }
      #We need to call super because Devise doesn't think we have sent any mail
      super
  end
end
