class Comment < ActiveRecord::Base
  acts_as_tree order: 'created_at DESC'

  belongs_to :commentable, polymorphic: true
  belongs_to :user
  before_create  :increment_counter
  before_destroy :decrement_counter

  scope :nested_all, ->() do
    hash_tree(limit_depth: 4)
  end

  def increment_counter
    klass = self.commentable_type.constantize
    klass.increment_counter(:comments_count, self.commentable_id)
  end

  def decrement_counter
    klass = self.commentable_type.constantize
    klass.decrement_counter(:comments_count, self.commentable_id)
  end

end
