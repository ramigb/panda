class AdminUser < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_accessor :name

  def login=(login)
    @login = login
  end

  def login
    @login || self.name
  end

  def self.find_for_database_authentication(warden_conditions)
    abort warden_conditions.inspect
    # where(conditions).where(["lower(name) = :value", { :value => warden_conditions.strip.downcase }]).first
  end

end
