class Post < ActiveRecord::Base
  include General

  acts_as_votable
  attr_accessor :tmp_img


  has_attached_file :image, :styles => { :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/


  scope :ordered, ->(order='hot',page=1,world_id = false) do
    q = where(deleted: 0)
    q = q.where(world_id: world_id) if world_id
    q = q.order(cached_weighted_total: :desc) if order == 'hot'
    q = q.order(created_at: :desc) if order == 'new'
    q = q.page(page)
    q = q.per(10)
    q = q.includes(:user)
    q = q.includes(:world)
  end

  belongs_to :world
  belongs_to :user
  has_many :comments, as: :commentable, dependent: :destroy
  has_one :content
  accepts_nested_attributes_for :content

  before_save :set_link_or_content
  before_save :create_slug


  validates :title, presence: true, length: { minimum: 3, maximum: 500 }
  validates :world_id, presence: true

  def to_param
    slug
  end

  def image_from_url(url)
    # Maybe the user specificed no image, and the link it self is an iamge!
    if self.link =~ /.jpg|.png|.gif|.jpeg/
      url = self.link if url.blank?
    end

    self.image = url unless url.blank?
    self.save!
  end
  handle_asynchronously :image_from_url, :run_at => Proc.new { 5.seconds.from_now }

  private

  def create_slug
    slug = Post.create_slug self.title
    if Post.where(slug: slug).first.nil?
      self.slug = slug
    else
      length = 4
      postfix = rand(36**length).to_s(36)
      self.slug = "#{slug}-#{postfix}"
    end
  end

  def set_link_or_content
    if self.content && self.content.body.length > 0
      self.is_local = true
      self.link = nil
    end
  end
end