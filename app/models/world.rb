class World < ActiveRecord::Base
  include General
  has_many :posts
  before_save :create_slug


  def create_slug
    self.slug = World.create_slug self.title
  end

  def to_param
    slug
  end



end
