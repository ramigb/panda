module ApplicationHelper
  def youtube_img url, version=0
    # This is an extract from auto_html to get the youtube img
    src = ""
    regex = /(https?:\/\/)?(www.)?(youtube\.com\/watch\?v=|youtu\.be\/|youtube\.com\/watch\?feature=player_embedded&v=)([A-Za-z0-9_-]*)(\&\S+)?(\?\S+)?/
    url.gsub(regex) do
      youtube_id = $4
      src = "#{$1}img.youtube.com/vi/#{youtube_id}/#{version}.jpg"
    end
    return src
  end
end
