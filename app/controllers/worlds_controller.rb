class WorldsController < ApplicationController
  respond_to :json

  def index
    @worlds = World.all
    respond_with @worlds
  end

  def show
    @world = World.where(slug: params[:id]).first
    respond_with @world
  end

end
