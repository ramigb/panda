class ApplicationController < ActionController::Base
  respond_to :html, :json

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  # before_action :allow_cross

  # def allow_cross
  #   headers['Access-Control-Allow-Origin'] = '*'
  #   headers['Access-Control-Request-Method'] = '*'
  # end
  after_filter :set_csrf_cookie_for_ng


  before_filter :configure_permitted_parameters, if: :devise_controller?

  def authenticate_admin_user!
    if current_user && current_user.admin
      return true
    else
      redirect_to '/'
    end
  end

  def current_admin_user
    redirect_to '/' if current_user.nil?
    return current_user unless !current_user.admin
  end

  def get_user
    respond_with current_user
  end

    before_filter :set_locale

  def set_locale
    I18n.locale = :en
  end





  def set_csrf_cookie_for_ng
    cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :email, :password, :password_confirmation, :remember_me) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :name, :email, :password, :remember_me) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name, :email, :password, :password_confirmation, :current_password) }
  end

  def verified_request?
    super || form_authenticity_token == request.headers['X-XSRF-TOKEN']
  end

end
