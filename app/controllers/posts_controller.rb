class PostsController < ApplicationController
  respond_to :json

  def vote
    slug = params[:id]
    @post = Post.where(slug: slug).first
    @post.liked_by current_user
    liked = @post.vote_registered?
    respond_with liked
  end

  def index
    page  = params[:page] ? params[:page] : 1
    order = params[:order] ? params[:order] : 'hot'

    if world = params[:world]
      @posts = Post.ordered(order,page,world)
    else
      @posts = Post.ordered(order,page)
    end

    respond_with @posts
  end

  def show
    slug = params[:id]
    @post = Post.where(slug: slug).first
    # respond_with @post, include: [{:content => {only: :body}}]
  end

  def create
    new_params = post_params
    image = new_params.delete(:image)
    @post = Post.create(new_params)
    @post.user_id = current_user.id
    @post.save
    @post.image_from_url(image)
    respond_with @post
  end

  private

  def post_params
    params.require(:post).permit(:title, :world_id, :link, :image, :content_attributes => [:body])
  end

end
