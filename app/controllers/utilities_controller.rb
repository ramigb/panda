class UtilitiesController < ApplicationController
  respond_to :json
  include ApplicationHelper

  def get_title
    link = params['link']
    unless link.blank?
      page = MetaInspector.new(link)
      title = page.title
    else
      title = ''
    end
    respond_with title
  end

  def get_thumb
    link = params['link']

    # What if the link it self is an image? go directly to it
    if link =~ /.jpg|.png|.gif|.jpeg/
        thumb = link
        return respond_with [thumb]
    end

    # If it's a Youtube video
    if link =~ /youtube/
      thumb = youtube_img link
      return respond_with [thumb]
    end

    # Else try to get anything from the page!
    unless link.blank?
      page = MetaInspector.new(link)
      thumb = page.images
    else
      thumb = ''
    end



    respond_with thumb
  end
end
