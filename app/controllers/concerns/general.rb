module General
  extend ActiveSupport::Concern

  included do
    def image_url file_name
      f = File.open(file_name, "r")
      self.image = f
      self.save
    end
  end

  # methods defined here are going to extend the class, not the instance of it
  module ClassMethods
    def create_slug string
      # Thanks to someone on github for posting this https://github.com/blazeeboy
      allowed = "1234567890qwertyuiopasdfghjklzxcvbnmإأآﻵذضصثقفغعهخحجدطكمنتالبيسشئءؤرﻻىةوزظ QWERTYUIOPLKJHGFDSAZXCVBNM"
      url = ''
      string.each_char {|c| url << c if allowed.include?(c)}
      url.chomp.gsub(' ', '-')
    end
  end

end